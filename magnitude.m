% if A is an m x n matrix, return an m x 1 column vector where each row is
% the magnitude of the corresponding row in A.

function mag = magnitude(A)
    rows = size(A, 1);
    mag = zeros(rows, 1);

    for index = 1:rows
        mag(index, 1) = sqrt(sum(A(index, :) .* A(index, :)));
    end
end