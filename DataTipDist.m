function [distances] = DataTipDist()
%// retrieve the data cursor manager for the figure
dcm = datacursormode(gcf) ;
%// get all the datatips information in a structure
dtip_info = dcm.getCursorInfo ;
%// extract the coordinates of each data tip from the structure
Coordinates_Datatip1 = dtip_info(1).Position ;
Coordinates_Datatip2 = dtip_info(2).Position ;
%// retrieve the data cursor manager for the figure
dcm = datacursormode(gcf) ;
%// get all the datatips information in a structure
dtip_info = dcm.getCursorInfo ;
%// extract the coordinates of each data tip from the structure
Coordinates_Datatip1 = dtip_info(1).Position ;
Coordinates_Datatip2 = dtip_info(2).Position ;
mag = magnitude(Coordinates_Datatip1 - Coordinates_Datatip2);

distances = [mag Coordinates_Datatip1 - Coordinates_Datatip2];