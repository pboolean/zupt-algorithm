orientation = [OrientationW OrientationX OrientationY OrientationZ];accel = [AccelXms2 AccelYms2 AccelZms2];
accel = [AccelXms2 AccelYms2 AccelZms2];
Time = (Timemillis - 82484) / 1000;

startOrientation = orientation(1, :);
startConj =[ startOrientation(1, 1) startOrientation(1, 2) -startOrientation(1, 3) -startOrientation(1, 4)];


orientation_rel = orientation;
rows = size(orientation_rel, 1);
for index = 1:rows
    orientation_rel(index, :) = quatmultiply(orientation_rel(index, :), startConj);
end

trimmed_orientation = orientation;%(736:7643, :);
trimmed_Time = Time;%(736:7643);
trimmed_accel = accel;%(736:7643, :);

% smoothed_accel = filter(filt, 1, trimmed_accel);

figure; plot(trimmed_Time, [(trimmed_accel(:, 1) +60) trimmed_accel(:, 2) (trimmed_accel(:, 3) - 60)]); legend ('x', 'y', 'z'); title('accel');

% figure; plot(trimmed_Time, [(smoothed_accel(:, 1) +60) smoothed_accel(:, 2) (smoothed_accel(:, 3) - 60)]); legend ('x', 'y', 'z'); title('smooth accel');

figure; plot(trimmed_Time, [(trimmed_orientation(:, 4) -3) trimmed_orientation(:, 3) (trimmed_orientation(:, 2) + 3) (trimmed_orientation(:, 1) + 6)]); legend ('w', 'x', 'y', 'z');

data.accel = trimmed_accel;
data.orientation = trimmed_orientation;
data.time = trimmed_Time;
data.primaryAxis = 'x';
data.mode = 'IMU'