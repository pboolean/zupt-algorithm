function Posns = ZuptPosn(drawerData, showFigs)

Time = drawerData.time;
accel = drawerData.accel;
orientation = drawerData.orientation;

WINDOW_SIZE = 7; % samples
STANCE_THRESH = 3;% 1.5; % m/s^2
STANCE_WIDTH = 5; %20; % samples (in 0.15 seconds)

orientation_inv = [orientation(:, 1), -orientation(:, 2), -orientation(:, 3), -orientation(:, 4)]; % X is flipped in the data coming off the BNO055

accel_global = quatrotate(orientation_inv, accel);
AccelXms2 = accel_global(:, 1);
AccelYms2 = accel_global(:, 2);
AccelZms2 = accel_global(:, 3);

accel_power = (AccelXms2 .* AccelXms2) + (AccelYms2 .* AccelYms2) + (AccelZms2 .* AccelZms2);
accel_power = accel_power';

isStance = zeros(1, size(accel_power, 2));
isMidStance = zeros(1, size(isStance, 2));

%window = zeros(1, WINDOW_SIZE);
stanceStart = -1;
stanceEnd = -1;
for sampleIndex = WINDOW_SIZE + 1:(size(accel_power, 2))
    window = accel_power((sampleIndex - WINDOW_SIZE):sampleIndex);
    if accel_power(sampleIndex) < STANCE_THRESH
        isStance(sampleIndex) = 1;
        
        if (not(isStance(sampleIndex -1))) % first stance sample
            stanceStart = sampleIndex;
            stanceEnd = -1;
        end
    else
        isStance(sampleIndex) = 0;
        
        if ((isStance(sampleIndex - 1))) % last stance sample
            stanceEnd = sampleIndex - 1;
            
            % only allow a minimum stance width
            if (stanceEnd - stanceStart < STANCE_WIDTH)
                isStance(stanceStart:stanceEnd) = 0; 
            else
                midStanceIndex = floor((stanceEnd + stanceStart) / 2);
                isMidStance(midStanceIndex) = true;
            end
            stanceStart = -1;
        end
    end
end

if not(-1 == stanceStart) %% ended during a stance phase
    midStanceIndex = floor((size(accel_power, 2) + stanceStart) / 2);
    isMidStance(midStanceIndex) = true;
end



isStance = isStance';        % column vector
isMidStance = isMidStance';  % column vector

if (showFigs)
figure; hold all; stanceArea = area(Time, 90*isStance); set(stanceArea, 'FaceColor','cyan'); plot(Time, 40*isMidStance); plot(Time, [(accel_global(:, 1) +90), (accel_global(:, 2) + 60), (accel_global(:, 3) + 0)]); hold off; legend('stance','midstance','x', 'y', 'z'); title(strcat('acceleration corrected to global reference frame - ', drawerData.mode, '_', drawerData.primaryAxis));
figure; hold all; stanceArea = area(Time, 90*isStance); set(stanceArea, 'FaceColor','cyan'); plot(Time, 40*isMidStance); plot(Time, [(accel(:, 1) +90), (accel(:, 2) +60), (accel(:, 3) +0)]); hold off; legend('stance','midstance','x', 'y', 'z'); title(strcat('uncorrected acceleration', drawerData.mode, '_', drawerData.primaryAxis));
end

gvt_raw_X = ZuptVelocity(AccelXms2, Time, isStance);

gvt_raw_Y = ZuptVelocity(AccelYms2, Time, isStance);

gvt_raw_Z = ZuptVelocity(AccelZms2, Time, isStance);

if (showFigs)
figure; plot(Time, [gvt_raw_X + 40, gvt_raw_Y + 20, gvt_raw_Z + 0]); legend('x', 'y', 'z'); title(strcat('velocity', drawerData.mode, '_', drawerData.primaryAxis));
end

% timeDelta = [diff(Time); 0.01];
% 
% posnX = cumsum(gvt_raw_X) .* timeDelta;
% posnY = cumsum(gvt_raw_Y) .* timeDelta;
% posnZ = cumsum(gvt_raw_Z) .* timeDelta;

deltaTime = [0; diff(Time)];
deltaTime(1) = deltaTime(2);
avg_time = cumsum(deltaTime);
div = 1:size(avg_time);
avg_time = avg_time ./ div';

posnX = cumsum(gvt_raw_X) .* avg_time;
posnY = cumsum(gvt_raw_Y) .* avg_time;
posnZ = cumsum(gvt_raw_Z) .* avg_time;


% riemann sum the velocities to get position
% posnX = cumsum(gvt_raw_X) .* 0.0069;
% posnY = cumsum(gvt_raw_Y) .* 0.0069;
% posnZ = cumsum(gvt_raw_Z) .* 0.0069;


% a = 0.0001;
% 
% posnX = filter([1-a a-1],[1 a-1], posnX);
% posnY = filter([1-a a-1],[1 a-1], posnY);
% posnZ = filter([1-a a-1],[1 a-1], posnZ);

if (showFigs)
figure;
hold all;
plot(Time, posnX);
plot(Time, posnY);
plot(Time, posnZ);
legend('x', 'y', 'z');
title(strcat('position', drawerData.mode, '_', drawerData.primaryAxis));

hold off;

% figure;
% hold all;
% plot(Time, posnY);
% plot(Time, posnY1);
% title('compare time methods');
% hold off;

figure;
plot3(posnX, posnY, posnZ);
title(strcat('position', drawerData.mode, '_', drawerData.primaryAxis));

end



% Posns = [posnX posnY posnZ];
Posns = [posnX posnY posnZ];
    end





