function [ gvt_raw ] = ZuptVelocity( accel, time, isStance )
%ZUPTVECTOR perform ZUPT algorithm on a single component of a vector to
%compute velocity
%   accel is a single component of an acceleration vector. it is a column
%   vector
%   time is a column vector containing timestamps for accel
%   isStance is a logical column vector representing whether a particular value of accel occurred during the stance phase.
%   repeatedly call this function for each component of a vector

gvt_raw = zeros(size(accel, 1), 1);

lastOffset = 0;

meanOffset = 0;
stanceSize = 0;

deltaTime = [0; diff(time)];
deltaTime(1) = deltaTime(2);
avg_time = cumsum(deltaTime);
div = 1:size(avg_time);
avg_time = avg_time ./ div';

for sampleIndex = 2:(size(gvt_raw, 1))
    if not(isStance(sampleIndex, 1))
        meanOffset = 0;
        stanceSize = 0;
        accel_val = accel(sampleIndex, 1);
        
        gvt_raw(sampleIndex, 1) = gvt_raw(sampleIndex -1, 1) + (accel_val - lastOffset)* (avg_time(sampleIndex)); %(time(sampleIndex, 1) - time(sampleIndex - 1, 1));  % riemann sum      

%         gvt_raw(sampleIndex, 1) = gvt_raw(sampleIndex -1, 1) + (accel_val - lastOffset)* (time(sampleIndex, 1) - time(sampleIndex - 1, 1)); %(time(sampleIndex, 1) - time(sampleIndex - 1, 1));  % riemann sum      
    else
        gvt_raw(sampleIndex, 1) = 0;
        meanOffset = meanOffset + (accel(sampleIndex, 1));
        stanceSize = stanceSize + 1;
        lastOffset = meanOffset / stanceSize;
    end
end
end

