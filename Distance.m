function [ dist ] = Distance( posn )
%DISTANCE Compute total distance traveled for a position trajectory
%   Compute magnitude total distance traveled for a position trajectory

rows = size(posn, 1);
firstPoint = posn(1, :);
% distX = 0;
% distY = 0;
% distZ = 0;
dist = 0;

for index = 2:rows
    point = posn(index, :);
    dist = dist + magnitude(point - firstPoint);
    
    firstPoint = point;
end



% dist = sqrt(distX^2 + distY^2 + distZ^2);

end

