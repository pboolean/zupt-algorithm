numSets = size(drawerData);
numSets = numSets(2);

spatialAxesLimits = [-2.5 1.5 -2 1 -2 1];

timeAxesLimits = [0 90 -2 2];

%% Everything (vs time)
figure;
title('Position Data (All)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    posn = ZuptPosn(drawerData{index}, false);
    posnPlot = plot(drawerData{index}.time, posn);
    set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.primaryAxis, ',', drawerData{index}.mode, ')' ));
    set(posnPlot(1), 'Color', 'blue');
    set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.primaryAxis, ',', drawerData{index}.mode, ')' ));
    set(posnPlot(2), 'Color', 'red');
    set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.primaryAxis, ',', drawerData{index}.mode, ')' ));
    set(posnPlot(3), 'Color', 'cyan');
end
hold off;

%% All (Spatial)
figure;
title('Position Data (All)');
axis(spatialAxesLimits);

hold all;
for index = 1:numSets
    posn = ZuptPosn(drawerData{index}, false);
    posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
    set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.primaryAxis, ',', drawerData{index}.mode, ')' ));
end
hold off;

%% IMU (vs time)
figure;
title('Position Data (IMU)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.mode, 'IMU')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot(drawerData{index}.time, posn);
        set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(1), 'Color', 'blue');
        set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(2), 'Color', 'red');
        set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(3), 'Color', 'cyan');
    end
end
hold off;

%% NDOF (vs time)
figure;
title('Position Data (NDOF)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.mode, 'NDOF')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot(drawerData{index}.time, posn);
        set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(1), 'Color', 'blue');
        set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(2), 'Color', 'red');
        set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        set(posnPlot(3), 'Color', 'cyan');
    end
end
hold off;
    

%% IMU (Spatial)
figure;
title('Position Data (IMU)');
axis(spatialAxesLimits);
hold all;
for index = 1:numSets
    if and(and(strcmp(drawerData{index}.mode, 'IMU'), not(index == 6)), and(not(index == 7), not(index == 15)))
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
        set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        
        if strcmp(drawerData{index}.primaryAxis, 'x')
            set(posnPlot, 'Color', 'red');
        else
            if strcmp(drawerData{index}.primaryAxis, 'y')
                set(posnPlot, 'Color', 'blue');
            else
                if strcmp(drawerData{index}.primaryAxis, 'z')
                    set(posnPlot, 'Color', 'cyan');
                end            
            end
        end
    end
end
hold off;

%% NDOF (Spatial)
figure;
title('Position Data (NDOF)');
axis(spatialAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.mode, 'NDOF')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
        set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.primaryAxis, ')' ));
        
        if strcmp(drawerData{index}.primaryAxis, 'x')
            set(posnPlot, 'Color', 'red');
        else
            if strcmp(drawerData{index}.primaryAxis, 'y')
                set(posnPlot, 'Color', 'blue');
            else
                if strcmp(drawerData{index}.primaryAxis, 'z')
                    set(posnPlot, 'Color', 'cyan');
                end            
            end
        end
    end
end
hold off;

%% X primary (vs time)
figure;
title('Position Data (X primary)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'x')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot(drawerData{index}.time, posn);
        set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(1), 'Color', 'blue');
        set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(2), 'Color', 'red');
        set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(3), 'Color', 'cyan');
    end
end
hold off;
    
%% Y primary (vs time)
figure;
title('Position Data (Y primary)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'y')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot(drawerData{index}.time, posn);
        set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(1), 'Color', 'blue');
        set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(2), 'Color', 'red');
        set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(3), 'Color', 'cyan');
    end
end
hold off;

%% Z primary (vs time)
figure;
title('Position Data (Z primary)');
axis(timeAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'z')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot(drawerData{index}.time, posn);
        set(posnPlot(1), 'DisplayName', strcat('X position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(1), 'Color', 'blue');
        set(posnPlot(2), 'DisplayName', strcat('Y position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(2), 'Color', 'red');
        set(posnPlot(3), 'DisplayName', strcat('Z position', num2str(index), '(',drawerData{index}.mode, ')' ));
        set(posnPlot(3), 'Color', 'cyan');
    end
end
hold off;


%% X primary (Spatial)
figure;
title('Position Data (X primary)');
axis(spatialAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'x')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
        set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.mode, ')' ));
        
        if strcmp(drawerData{index}.mode, 'IMU')
            set(posnPlot, 'Color', 'red');
        else
            if strcmp(drawerData{index}.mode, 'NDOF')
                set(posnPlot, 'Color', 'blue');
            end  
        end
    end
end

hold off;

%% Y primary (Spatial)
figure;
title('Position Data (Y primary)');
axis(spatialAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'y')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
        set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.mode, ')' ));
        
        if strcmp(drawerData{index}.mode, 'IMU')
            set(posnPlot, 'Color', 'red');
        else
            if strcmp(drawerData{index}.mode, 'NDOF')
                set(posnPlot, 'Color', 'blue');
            end  
        end
    end
end
hold off;


%% Z primary (Spatial)
figure;
title('Position Data (Z primary)');
axis(spatialAxesLimits);
hold all;
for index = 1:numSets
    if strcmp(drawerData{index}.primaryAxis, 'z')
        posn = ZuptPosn(drawerData{index}, false);
        posnPlot = plot3(posn(:, 1), posn(:, 2), posn(:, 3));
        set(posnPlot, 'DisplayName', strcat('position', num2str(index), '(',drawerData{index}.mode, ')' ));
        
        if strcmp(drawerData{index}.mode, 'IMU')
            set(posnPlot, 'Color', 'red');
        else
            if strcmp(drawerData{index}.mode, 'NDOF')
                set(posnPlot, 'Color', 'blue');
            end  
        end
    end
end
hold off;
