function [ distances ] = PlotNav( walkingData, overlay, scale, originX, originY )
%PLOTNAV Summary of this function goes here
%   Detailed explanation goes here

numData = size(walkingData, 2);

scaledPosns = cell(1, numData);

distances = zeros(numData, 3);

for index = 1:numData
    
    walkData = walkingData{index};
    
    posn = ZuptPosn(walkData, false);
    startOrientation = walkData.orientation(1, :);
%     startConj =[ startOrientation(1, 1) -startOrientation(1, 2) -startOrientation(1, 3) -startOrientation(1, 4)];
        
    % scale up
    scaledPosn = posn * scale;
%     scaledPosn = quatrotate(startConj, scaledPosn);


<<<<<<< HEAD
    % rotate non-magnetometer plots
%     angle = 0;
%         switch (index)
%         case 1
%             angle = 50;
%             
% %         case 2
% %             angle = -75;
%             
%         case 2
%             angle = 163;
%             
%         case 3
%             angle = -57;
%         
%         case 4
%             angle = -155;
%             
%         case 5
%             angle = -150;
%             scaledPosn = posn * scale * 1.15;
%             
%         case 7
%             angle = 170;
%         end

    % rotate magnetometer plots
    switch (index)
        case 1
            angle = -40;
            
        case 2
            angle = -60;
            
        case 3
            angle = -35;
            
        case 4
            angle = -40;
    end
=======
    % rotate
    angle = 0;
        switch (index)
        case 1
            angle = 50;
            
%         case 2
%             angle = -75;
            
        case 2
            angle = 163;
            
        case 3
            angle = -57;
        
        case 4
            angle = -155;
            
        case 5
            angle = -150;
            scaledPosn = posn * scale * 1.15;
            
        case 7
            angle = 170;
        end

    
%     switch (index)
%         case 1
%             angle = -40;
%             
%         case 2
%             angle = -60;
%             
%         case 3
%             angle = -35;
%             
%         case 4
%             angle = -40;
%     end
>>>>>>> origin/master
            
            
    angle_rad = angle * pi / 180;
    scaledPosn = rodrigues_rot(scaledPosn, [0 0 1], angle_rad);
    
    % translate to origin in image
    scaledPosn(:, 1) = scaledPosn(:, 1) + originX;
    scaledPosn(:, 2) = -scaledPosn(:, 2) + originY;
    scaledPosn(:, 3) = scaledPosn(:, 3) + 500;
    
    scaledPosns{index} = scaledPosn;    
    
%     distances(index) = Distance(posn);
    lastPosn = size(posn, 1);
    distances(index, :) = posn(lastPosn, :);
end

figure;
imshow(overlay + 200);
hold all;
for index = 1:numData
    
    posnPlot = plot3(scaledPosns{index}(:, 1), scaledPosns{index}(:, 2), scaledPosns{index}(:, 3), 'LineWidth', 5);
%     rotate(posnPlot, [0 0 1], -40)

end
hold off;

end

