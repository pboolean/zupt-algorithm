# README #

This MATLAB code implements the ZUPT algorithm.


### How do I use this? ###

1. Load Data\walking\walkabout_0.mat into your MATLAB workspace
2. Type `posn = ZuptPosn(walkabout_0, true);` and press enter. This will run the ZUPT algorithm on some sample data.
2. load 5th_floor_map.png into your MATLAB workspace (it should be named *x5th_floor_map*)
3. Type `PlotNav({walkabout_0}, x5th_floor_map, 10.4987, 390, 490)` and press enter. This will runthe ZUPT algorithm and superimpose it on the map of where the trajectory was collected.

4. load Data/walking/imu_xprimary_walkabout.csv into your MATLAB workspace.
5. For import settings, make sure that 'import as column vectors' is selected, and that you have selected to 'remove unimportable data'
6. run the 'zupt' script to process the column vectors into a struct called 'data'
7. type ` posn = ZuptPosn(data, true);` to process the data.